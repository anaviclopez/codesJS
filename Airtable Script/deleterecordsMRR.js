
//// Deleting Duplicate Records ////

let settings = {
  table: MRRTable,
  firstIdField: MRRTable.getField("Periodo"),
  secondIdField: MRRTable.getField("Organizacion"),
  comparisonField: MRRTable.getField("ID"),
};

let { table, firstIdField, secondIdField, comparisonField } = settings;
let maxRecordsPerCall = 50;

function choose(recordA, recordB) {
  let valueA = recordA.getCellValueAsString(comparisonField);
  let valueB = recordB.getCellValueAsString(comparisonField);
  return valueA > valueB
    ? { keep: recordA, discard: recordB }
    : { keep: recordB, discard: recordA };
}

let existing = Object.create(null);
let toDelete = [];

let query = await table.selectRecordsAsync({
  fields: [firstIdField, secondIdField, comparisonField],
});

for (let record of query.records) {
  let key = JSON.stringify([
    record.getCellValue(firstIdField),
    record.getCellValue(secondIdField),
  ]);

  if (key in existing) {
    let { keep, discard } = choose(record, existing[key]);
    toDelete.push(discard);
    existing[key] = keep;
  } else {
    existing[key] = record;
  }
}
//console.log(`Identified **${toDelete.length}** records in need of deletion.`);
while (toDelete.length > 0) {
  await table.deleteRecordsAsync(toDelete.slice(0, maxRecordsPerCall));
  toDelete = toDelete.slice(maxRecordsPerCall);
}