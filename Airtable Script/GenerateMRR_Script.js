// What we need
let ContractsTable = base.getTable("Contratos");
let OrganizationsTable = base.getTable("Organizaciones");
let MRRTable = base.getTable("MRR");
let ContractContent = await ContractsTable.selectRecordsAsync();
let OrganizationContent = await OrganizationsTable.selectRecordsAsync();

class MRR_Manager {
  constructor(Period, OrganizationRecord) {
    this.Period = new Date(Period);
    this.OrganizationRecord = OrganizationRecord;
  }
  async StopForDebug(message, objectToPrint = null) {
    if (objectToPrint) {
      output.inspect(objectToPrint);
    }
    let waga = await input.textAsync(message);
  }
  async GetContractsOrganization() {
    let ContractRecordsInOrganization = [];
    if (this.OrganizationRecord.getCellValue("Contratos") != null) {
      ContractRecordsInOrganization =
        this.OrganizationRecord.getCellValue("Contratos");
    }
    return ContractRecordsInOrganization;
  }
  async GetInitialDateinPeriod(ContractRecordsinOrganization) {
    /*
    let GetInitialDatesinPeriod = ContractRecordsinOrganization.map((record) =>
      ContractContent.getRecord(record.id).getCellValue("Fecha Inicio")
    ).filter((record) => new Date(record) <= this.Period);
    let InitialDateinPeriod = [];
    if (GetInitialDatesinPeriod.length) {
      InitialDateinPeriod.push(
        GetInitialDatesinPeriod[GetInitialDatesinPeriod.length - 1]
      );
    }*/
 // }
  //async GetContractinPeriod(InitialDateinPeriod) {
    let ContractinPeriod = ContractContent.records
      .filter(
        (record) => record.getCellValue("Fecha Inicio") <= this.Period
      )
      .filter(
        (record) =>
          new Date(record.getCellValue("Fecha de Corte")) > this.Period
      )
      .filter((record) =>
        record
          .getCellValue("ID Contrato")
          .includes(this.OrganizationRecord.getCellValue("Clave"))
      );

    return ContractinPeriod;
  }

  async GetMRR(ContractinPeriod) {
    let Record = ContractinPeriod[0];
    let DataMRR = Record.getCellValue("Total Precio Final Mensual");

    return DataMRR;
  }

  async CreateMRR(DataMRR) {
    return await MRRTable.createRecordAsync({
      Periodo: this.Period,
      Organizacion: [{ id: this.OrganizationRecord.id }],
      "Valor MRR": DataMRR,
    });
  }

  async Process() {
    const ContractRecordsInOrganization = await this.GetContractsOrganization();

    if (ContractRecordsInOrganization.length) {
      const InitialDateinPeriod = await this.GetInitialDateinPeriod(
        ContractRecordsInOrganization
      );
      //await this.StopForDebug("waga",InitialDateinPeriod)
      if (InitialDateinPeriod.length) {
        const ContractinPeriod = await this.GetContractinPeriod(
          InitialDateinPeriod
        );
        if (ContractinPeriod.length) {
          const DataMRR = await this.GetMRR(ContractinPeriod);
          await this.CreateMRR(DataMRR);
        }
      }
    }
  }
}

class DatesinArray {
  constructor(date) {
    this.date = date;
  }
  async Process() {
    var DateToday = new Date();
    var MonthToday = DateToday.getMonth() + 1;
    var YearToday = DateToday.getFullYear().toString();
    var StartDate = this.date.split("-");
    var counter = parseInt(StartDate[1]);
    var Dates = [];
    for (var i = parseInt(StartDate[0]); i <= parseInt(YearToday); i++) {
      for (var j = counter; j <= 12; j++) {
        if (j > MonthToday && i == YearToday) {
          continue;
        }
        Dates.push(i + "-" + j + "-" + 1);
      }
      counter = 1;
    }
    return Dates;
  }
}

/* We define the initial Date
var date = new DatesinArray('2014-01-01');

let Datesinarray = await date.Process();
*/
// Loop for all Organizations
for (let i = 0; i < OrganizationContent.records.length; i++) {
  let OrganizationRecord = OrganizationContent.records[i];
  /*
  for (var j = 0; j < Datesinarray.length; j++) {
  var Period = Datesinarray[j];
  */
  var Period = new Date().setDate(1);
  const MRR = new MRR_Manager(Period, OrganizationRecord);
  await MRR.Process();
  //}
}

/*
//// Deleting Duplicate Records ////

let settings = {
  table: MRRTable,
  firstIdField: MRRTable.getField("Periodo"),
  secondIdField: MRRTable.getField("Organizacion"),
  comparisonField: MRRTable.getField("ID"),
};

let { table, firstIdField, secondIdField, comparisonField } = settings;
let maxRecordsPerCall = 50;

function choose(recordA, recordB) {
  let valueA = recordA.getCellValueAsString(comparisonField);
  let valueB = recordB.getCellValueAsString(comparisonField);
  return valueA > valueB
    ? { keep: recordA, discard: recordB }
    : { keep: recordB, discard: recordA };
}

let existing = Object.create(null);
let toDelete = [];

let query = await table.selectRecordsAsync({
  fields: [firstIdField, secondIdField, comparisonField],
});

for (let record of query.records) {
  let key = JSON.stringify([
    record.getCellValue(firstIdField),
    record.getCellValue(secondIdField),
  ]);

  if (key in existing) {
    let { keep, discard } = choose(record, existing[key]);
    toDelete.push(discard);
    existing[key] = keep;
  } else {
    existing[key] = record;
  }
}
//console.log(`Identified **${toDelete.length}** records in need of deletion.`);
while (toDelete.length > 0) {
  await table.deleteRecordsAsync(toDelete.slice(0, maxRecordsPerCall));
  toDelete = toDelete.slice(maxRecordsPerCall);
}*/