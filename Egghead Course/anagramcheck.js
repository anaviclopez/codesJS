const anagramOne = 'Astronomer';
const anagramTwo = 'moon Starer';
const notAnagram = 'hello world';

const anagramCheck = (stringOne,stringTwo)=> {
const modifyString = string => string
.toLowerCase()
.split('')
.sort()
.filter(char=>char.match(/[a-zA-Z]/))
.join('');
return modifyString(stringOne) === modifyString(stringTwo);
}

console.log(anagramCheck(anagramOne,notAnagram));