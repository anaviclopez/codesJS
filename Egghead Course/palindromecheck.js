const testString ='Rotor';
const notPalindrome ='teststring';

// Forma #1
const palindromeCheck = string => {
const lowerCaseString = string.toLowerCase();

const reversedString = lowerCaseString
    .split('')
    .reverse()
    .join('')
    console.log(reversedString);
return lowerCaseString === reversedString;
}

// Forma #2
console.log(palindromeCheck(notPalindrome));

const palindromeCheck1 = string => string.toLowerCase() === string.toLowerCase().split('').reverse().join('');

console.log(palindromeCheck1(testString));
    