const nestedArray=[1,2,[3],[4,[5]]];

const flattenarray = array =>{
    return array.reduce(
        (accumulator,iterationValue) => 
        Array.isArray(iterationValue)
        ? [...accumulator,...flattenarray(iterationValue)]
        : [...accumulator,iterationValue],
        []

        );
};

console.log(flattenarray(nestedArray));
