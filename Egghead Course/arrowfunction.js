const addFive = function (num) {
    return num + 5;
};

console.log(addFive(5));

const addFiveArrow = num =>{
return num + 5;
};

const addFiveArrowImplicit = num => num + 5;
const noArgArrow = () => true;
const coupleArgArrow = (argOne,argTwo) => argOne + argTwo;

console.log(addFiveArrow(5));
console.log(addFiveArrowImplicit(10));
console.log(noArgArrow());
console.log(coupleArgArrow(2,3));


