const entero = -42442;

const reverseInt = (int) => {
const reversedString = int.toString().split('').reverse().join('');
return parseInt(reversedString) * Math.sign(int);

}

console.log(reverseInt(entero));